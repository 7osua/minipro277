package com.example.minipro.Controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(value = "/test/")
public class MainController {

    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("test/index");
        return view;
    }
}
